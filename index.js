let pElems = document.getElementsByTagName('p');

for(i = 0; i < pElems.length; i++){
    pElems[i].style.backgroundColor = "#ff0000";
}

let elementsOfOptionsList = document.getElementById("optionsList");

let testParagraphElement = document.getElementById("testParagraph");

testParagraphElement.textContent = "This is a paragraph";

console.log(elementsOfOptionsList);

console.log(elementsOfOptionsList.parentElement);

console.log(elementsOfOptionsList.childNodes);

let mainHeaderElements = document.querySelector(".main-header");

let innerElements = mainHeaderElements.querySelectorAll('*');

for(i = 0; i < innerElements.length; i++){
    console.log(innerElements[i])
    innerElements[i].classList.add('nav-item')
}

let sectionTitleElements = document.querySelectorAll(".section-title")

for(i = 0; i < sectionTitleElements.length; i++){
    console.log(sectionTitleElements[i]);
    sectionTitleElements[i].classList.remove("section-title");
    console.log(sectionTitleElements[i]);
}